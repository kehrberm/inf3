import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class ParserTest {

    @Test
    public void testPointBeforeLine() throws Exception {

        Lexer lexer = new Lexer();

        ArrayList<Token> tokens = lexer.lex("3*4+5");

        Parser parser = new Parser();

        AstExpression root =  parser.parse(tokens);

        assertEquals(root.astBinaryOp.astExpression1.astBinaryOp.astExpression1.astValue.astNumber.astDigitWoz.astDigitWozContent, 3);
        assertEquals(root.astBinaryOp.astExpression1.astBinaryOp.astExpression2.astValue.astNumber.astDigitWoz.astDigitWozContent, 4);
        assertEquals(root.astBinaryOp.astExpression2.astValue.astNumber.astDigitWoz.astDigitWozContent, 5);

    }

    @Test
    public void testPowerBeforePoint() throws Exception {

        Lexer lexer = new Lexer();

        ArrayList<Token> tokens = lexer.lex("12+3^4*5");

        Parser parser = new Parser();

        AstExpression root =  parser.parse(tokens);

        assertEquals(root.astBinaryOp.astExpression2.astBinaryOp.astExpression1.astBinaryOp.
                astExpression1.astValue.astNumber.astDigitWoz.astDigitWozContent, 3);
        assertEquals(root.astBinaryOp.astExpression2.astBinaryOp.astExpression1.astBinaryOp.
                astExpression2.astValue.astNumber.astDigitWoz.astDigitWozContent, 4);

    }

}
