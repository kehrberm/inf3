import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.stream.IntStream;

public class LexerTest {

    @Test
    @DisplayName("simpleExpression")
    public void testSimpleExpression() {

        Lexer lexer = new Lexer();

        String expression = "5";

        ArrayList<Token> lexResult = lexer.lex(expression);

        ArrayList<Token> lexExpectedResult = new ArrayList<>();
        lexExpectedResult.add(new Token(TokenType.number, "5"));


        assertEquals(lexExpectedResult.get(0).tokenType, lexResult.get(0).tokenType);
        assertEquals(lexExpectedResult.get(0).tokenString, lexResult.get(0).tokenString);
    }

    @Test
    public void testComplicatedExpression() {
        Lexer lexer = new Lexer();

        String expression2 = "((22+3x)*5/3)";
        ArrayList<Token> lexResult2 = lexer.lex(expression2);

        ArrayList<Token> lexExpectedResult2 = new ArrayList<>();
        lexExpectedResult2.add(new Token(TokenType.special, "("));
        lexExpectedResult2.add(new Token(TokenType.special, "("));
        lexExpectedResult2.add(new Token(TokenType.number, "22"));
        lexExpectedResult2.add(new Token(TokenType.special, "+"));
        lexExpectedResult2.add(new Token(TokenType.number, "3"));
        lexExpectedResult2.add(new Token(TokenType.var, "x"));
        lexExpectedResult2.add(new Token(TokenType.special, ")"));
        lexExpectedResult2.add(new Token(TokenType.special, "*"));
        lexExpectedResult2.add(new Token(TokenType.number, "5"));
        lexExpectedResult2.add(new Token(TokenType.special, "/"));
        lexExpectedResult2.add(new Token(TokenType.number, "3"));
        lexExpectedResult2.add(new Token(TokenType.special, ")"));

        for (int i = 0; i < lexResult2.size() ; i++) {
            assertEquals(lexResult2.get(i).getTokenString(), lexExpectedResult2.get(i).getTokenString());
            assertEquals(lexResult2.get(i).getTokenType(), lexExpectedResult2.get(i).getTokenType());
        }

    }



}

