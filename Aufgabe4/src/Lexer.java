import java.util.ArrayList;
import java.util.Objects;

import static org.valid4j.Assertive.ensure;
import static org.valid4j.Assertive.require;


public class Lexer {




    public ArrayList<Token> lex(String s) {
        require(s != null);
        require(s.length() > 0);


        ArrayList<String> separatedChars = separateChars(s);

        ArrayList<String> tokensAsStrings = buildSubstringsFromSeparatedChars(separatedChars);

        ArrayList<Token> tokenList = constructTokenList(tokensAsStrings);

        ensure(tokenList.size() > 0);
        return tokenList;
    }



    // separates char with "|" symbol
    // if char is a representation of an integer: doesn't add separator symbol
    private ArrayList<String> separateChars(String stringToTokenize) {

        ArrayList<String> separated = new ArrayList<>();

        separated.add("|");

        for (char c : stringToTokenize.toCharArray()) {

            if(Character.isWhitespace(c)){
                continue;
            }

            if(Character.isDigit(c)) {
                separated.add( String.valueOf(c) );
            } else {
                separated.add("|");
                separated.add( String.valueOf(c) );
                separated.add("|");
            }
        }

        separated.add("|");

        return separated;
    }


    // takes separated chars and builds an arrayList with substrings
    // values are put into the same substring if both represent integers and are directly after each other
    private ArrayList<String> buildSubstringsFromSeparatedChars(ArrayList<String> separated) {

        ArrayList<String> tokensAsStrings = new ArrayList<>();

        StringBuilder temp = new StringBuilder();

        for(String c : separated) {
            if(Objects.equals(c, "|")) {

                if(!temp.toString().equals("")) {
                    tokensAsStrings.add(temp.toString());
                }

                temp = new StringBuilder();

            } else {
                temp.append(c);
            }
        }

        return tokensAsStrings;

    }

    // substrings get parsed into tokens, if substring is an integer: create number token
    // if substring is a character: create var token, otherwise: create special token
    private ArrayList<Token> constructTokenList(ArrayList<String> tokensAsStrings) {

        ArrayList<Token> tokens = new ArrayList<>();

        boolean notANumber = false;

        for(String s1 : tokensAsStrings) {

            try {
                Integer.parseInt(s1);
                tokens.add(new Token(TokenType.number, s1));
            } catch (NumberFormatException ignored) {

                notANumber = true;

            }

            if(notANumber) {

                if (Character.isLetter(s1.charAt(0))) {
                    tokens.add(new Token(TokenType.var, s1));
                } else {
                    tokens.add(new Token(TokenType.special, s1));
                }
                notANumber = false;
            }
        }

        return tokens;
    }
}
