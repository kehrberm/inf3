public class Exceptions {

    public static Class<? extends Throwable> ParserException;

    public static class ParserException extends RuntimeException {
        private String message;

        public ParserException(String string) {
            this.message = string;
        }

        public String toString() {
            return "parser failed: " + message;
        }

        /**
         *
         */
        private static final long serialVersionUID = 1L;

    }


    @SuppressWarnings("serial")
    public static class LexerException extends RuntimeException {
        public LexerException() {
        }

        public LexerException(String string) {
            super(string);
        }
    }
}