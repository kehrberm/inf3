import javax.management.BadAttributeValueExpException;

import static org.valid4j.Assertive.*;


public class Evaluator {

    private float counter;

    // sets current value that variables are replaced with
    public void setCounter(float counter) {
        this.counter = counter;
    }



    public float visit(AstExpression node) {

        // if expression has not null expression as attribute: visit this expression
        if(node.astExpression != null) {

            return visit(node.astExpression);

        }
        // if expression has not null binary op as attribute: visit binary op
        else if(node.astBinaryOp != null) {

            return visit(node.astBinaryOp);

        }

        // visit value
        else {
            try {
                return visit(node.astValue);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                throw e;
            }
        }


    }

    public float visit(AstBinaryOp node) {

        // visits both expressions, does math according to op operator
        switch(node.astOperator.astOperatorContent) {

            case "+":
                return visit(node.astExpression1) + visit(node.astExpression2);
            case "-":
                return visit(node.astExpression1) - visit(node.astExpression2);
            case "*":
                return visit(node.astExpression1) * visit(node.astExpression2);
            case "/":
                return visit(node.astExpression1) / visit(node.astExpression2);
            case "^":
                return (float) Math.pow(visit(node.astExpression1), visit(node.astExpression2));
        }

        neverGetHere();
        return -111;
    }


    // returns value, parses depending on if variable, number or decimal
    public float visit(AstValue node) {

        if(node.astVariable != null) {

            return visit(node.astVariable);

        } else if (node.astNumber != null){

            return visit(node.astNumber);

        } else {

            return Float.parseFloat(visit(node.astDecimal.astDigitBeforeComma)
                    + "."
                    + visit(node.astDecimal.astDigitAfterComma));
        }
    }

    public int visit(AstNumber node) {

        return node.astDigitWoz.astDigitWozContent;

    }


    // returns current counter as variable value
    public float visit(AstVariable node) {

        return counter;

    }

}
