import java.util.ArrayList;

// class is used to transfer function values from main method to plotter
public class ValuesToDraw {

    private ArrayList<int[]> xCordArrays;

    int i = 0;

    ValuesToDraw(ArrayList<int[]> xCordArrays) {
        this.xCordArrays = xCordArrays;

    }

    public int[] getXCords() {

        if(i > xCordArrays.size() - 1)
            i = 0;

        return xCordArrays.get(i++);

    }

    public int[] getYCords () {

        int[] yCords = new int[]{-20, -19, -18, -17, -16, -15, -14, -13, -12, -11, -10,
                -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
                10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};

        return yCords;

    }

}
