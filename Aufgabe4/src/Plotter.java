import javax.swing.SwingUtilities;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.util.Arrays;

public class Plotter {


    public static void plot(ValuesToDraw valuesToDraw) {


        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                createAndShowGUI(valuesToDraw);
            }

        });
    }



    public static void createAndShowGUI(ValuesToDraw valuesToDraw) {

        JFrame f = new JFrame("Function Plotter");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.add(new PlotterPanel(valuesToDraw));
        f.setLocationRelativeTo(null);
        f.pack();
        f.setVisible(true);

    }
}

class PlotterPanel extends JPanel {

    // int array placeholders that get written into the values that should be displayed

    int[] xCords = new int[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    int[] yCords = new int[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};


    // offset values to have coordinates appear in the middle of a 800x600 window
    final int ZERO_POINT_Y = 300;
    final int ZERO_POINT_X = 400;

    ValuesToDraw valuesToDraw;



    PlotterPanel(ValuesToDraw valuesToDraw) {
        this.valuesToDraw = valuesToDraw;



        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {


                int[] yCordsWithOffset = new int[41];
                int[] xCordsWithOffset = new int[41];


                int i = 0;

                // gets values to draw from valuesToDraw object

                int[] xCords = valuesToDraw.getXCords();

                // offsets values to print in screen middle

                for(int cord : xCords) {
                    yCordsWithOffset[i] = (ZERO_POINT_Y + (cord * -1 * 10));
                    i++;
                }


                int j = 0;

                int[] yCords = valuesToDraw.getYCords();

                // offsets values to print in screen middle

                for(int cord : yCords) {
                    xCordsWithOffset[j] = ZERO_POINT_X + (cord * 10);
                    j++;
                }


                // writes values into classes attribute

                PlotterPanel.this.yCords = yCordsWithOffset;
                PlotterPanel.this.xCords = xCordsWithOffset;


                // update to show updated attributes

                updateUI();
            }
        });

    }




    public Dimension getPreferredSize() {
        return new Dimension(800,600);
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);


        //paints coordinate system

        // draw x and y axis
        g.drawLine(400, 50, 400, 550);
        g.drawLine(150, 300, 650, 300);

        //x=0, y=20
        g.drawLine(395, 100, 405, 100);
        //x=0, y=-20
        g.drawLine(395, 500, 405, 500);
        //x=-20, y=0
        g.drawLine(200,295 , 200, 305);
        //x=20, y=0
        g.drawLine(600,295 , 600, 305);


        //paints the lines that are in xCords and yCords

        drawLine(g);


    }


    private void drawLine(Graphics g){


        // gets classes attributes and paints them

        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.RED);

        g2d.drawPolyline(xCords, yCords, 41);


    }



}


