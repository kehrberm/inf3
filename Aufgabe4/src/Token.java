public class Token {

    TokenType tokenType;
    String tokenString;

    Token(TokenType tokenType, String tokenString) {

        this.tokenType = tokenType;
        this.tokenString = tokenString;

    }

    public TokenType getTokenType() {
        return tokenType;
    }

    public void setTokenType(TokenType tokenType) {
        this.tokenType = tokenType;
    }

    public String getTokenString() {
        return tokenString;
    }

    public void setTokenString(String tokenString) {
        this.tokenString = tokenString;
    }
}
