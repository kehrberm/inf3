import java.util.ArrayList;

public abstract class Ast {

    public void visit(){

    }

}

class AstExpression extends Ast {

    AstExpression astExpression;
    AstBinaryOp astBinaryOp;
    AstValue astValue;
    
}

class AstBinaryOp extends Ast {

    AstExpression astExpression1;
    AstExpression astExpression2;
    AstOperator astOperator;


}

class AstValue extends Ast {

    AstNumber astNumber;
    AstDecimal astDecimal;
    AstVariable astVariable;
}

class AstVariable extends Ast {

    String astVariable;

}

class AstNumber extends Ast {

    AstDigitWoZ astDigitWoz;
    ArrayList<AstDigit> astDigits = new ArrayList<>();

}

class AstDigitWoZ extends Ast {

    int astDigitWozContent;

}

class AstDigit extends Ast {

    AstDigitWoZ astDigitWoZ;
    int astDigit;

}

class AstDecimal extends Ast {

    AstNumber astDigitBeforeComma;
    AstNumber astDigitAfterComma;


}

class AstOperator extends Ast {

    String astOperatorContent;
}
