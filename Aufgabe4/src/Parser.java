import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import static org.valid4j.Assertive.*;

public class Parser {

    // parses the initial list of tokens
    public AstExpression parse(ArrayList<Token> tokenList) throws Exception {
        require(tokenList.size() > 0);

        AstExpression root;

        root = parseAstExpression(tokenList);

        ensure(root.astBinaryOp != null || root.astExpression != null || root.astValue != null);
        return root;
    }


    private AstExpression parseAstExpression (ArrayList<Token> tokenList) throws Exception {

        AstExpression toReturn = new AstExpression();

        // If tokenList contains one item: Expression is a value

        boolean isValue = false;

        if(tokenList.size() == 1 ) {

            toReturn.astValue = parseValue(tokenList);

            return toReturn;

        }

        // If tokenList contains a dot and no operators (+ - * /): Expression is a value
        for (Token t : tokenList) {


            if(t.getTokenString().equals(".")) {
                isValue = true;
            }


            if(t.getTokenString().equals("+") || t.getTokenString().equals("-") ||
                    t.getTokenString().equals("*") || t.getTokenString().equals("/") ||
                    t.getTokenString().equals("^")) {
                isValue = false;
                break;
            }

        }

        if(isValue) {
            toReturn.astValue = parseValue(tokenList);
            return toReturn;
        }


        // If method reaches this point: Expression must be expression in brackets or binary operation

        // Check if first and last tokens have "(" and ")" strings

        try {

            if (tokenList.get(0).tokenString.equals("(") && tokenList.get(tokenList.size() - 1).tokenString.equals(")")) {


                // Check if "false alarm", e.g. ( 1 + 2 ) + ( 1 + 2 ) <- starts and ends with ( and ) but needs to be treated as binaryOp

                boolean falseAlarm = false;

                for (int i = tokenList.size() - 2; i > 0; i--) {

                    if (tokenList.get(i).tokenString.equals(")") || tokenList.get(i).tokenString.equals("(")) {

                        if (tokenList.get(i).tokenString.equals(")")) {
                            falseAlarm = false;

                        } else {
                            falseAlarm = true;
                        }
                        break;
                    }
                }

                // Parse tokenList without first and last object and save it in toReturns astExpression

                if (!falseAlarm) {

                    toReturn.astExpression = parseAstExpression(
                            (ArrayList<Token>) IntStream.range(1, tokenList.size() - 1)
                                    .mapToObj(i -> tokenList.get(i))
                                    .collect(Collectors.toList()) // https://www.baeldung.com/java-stream-indices
                    );
                    return toReturn;
                }

            }

        } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
            throw new Exceptions.ParserException("String is not a valid function");
        }


        toReturn.astBinaryOp = parseBinaryOp(tokenList);

        return toReturn;

    }



    private AstBinaryOp parseBinaryOp (ArrayList<Token> tokenList) throws Exception {


        AstBinaryOp toReturn = new AstBinaryOp();

        int positionOfRelevantOperator = 0;
        int numberOfBrackets = 0;
        int i = 0;
        String lastBracket = ")";

        int positionOfFirstTopLevelAdditionOrSubtraction = 0,
                positionOfFirstTopLevelMultiplicationOrDivision = 0, positionOfFirstTopLevelPower = 0;


        boolean flagAdditionSubtraction = false, flagMultiplicationDivision = false, flagPower = false;


        // this for loop determines the first operator that is not in any brackets
        for(Token t : tokenList) {


            // keeps track of number of brackets and saves current bracket to lastBracket
            if(t.getTokenString().equals("(") || t.getTokenString().equals(")")) {
                numberOfBrackets++;
                lastBracket = t.tokenString;
            }

            // if last bracket is ")" and numberOfBrackets is divisible by 2 (meaning we are on the top level): activate AdditionSubtraction flag and save the operator
            if( ((t.getTokenString().equals("+") || t.getTokenString().equals("-") && (numberOfBrackets % 2 == 0) ) && lastBracket.equals((")"))) ) {

                if(!flagAdditionSubtraction) {
                    flagAdditionSubtraction = true;
                    positionOfFirstTopLevelAdditionOrSubtraction = i;
                }

            }

            // if last bracket is ")" and numberOfBrackets is divisible by 2: activate Multiplication flag and save the operator
            if( ((t.getTokenString().equals("*") || t.getTokenString().equals("/")) && (numberOfBrackets % 2 == 0) ) && lastBracket.equals((")") )) {

                if(!flagMultiplicationDivision) {
                    flagMultiplicationDivision = true;
                    positionOfFirstTopLevelMultiplicationOrDivision = i;
                }

            }

            // if last bracket is ")" and numberOfBrackets is divisible by 2: activate power flag and save the operator
            if( t.getTokenString().equals("^") && (numberOfBrackets % 2 == 0) && lastBracket.equals((")") )) {

                if(!flagPower) {
                    flagPower = true;
                    positionOfFirstTopLevelPower = i;
                }
            }

            i++;
        }

        // if there is an addition operator on the top level: split expression at this operator
        // if no additionOperator and there is a multiplication operator: split Exp at multp operator
        // else: split at power operator
        if(flagAdditionSubtraction) {
            positionOfRelevantOperator = positionOfFirstTopLevelAdditionOrSubtraction;

        } else if (flagMultiplicationDivision){
            positionOfRelevantOperator = positionOfFirstTopLevelMultiplicationOrDivision;
        } else if (flagPower) {
            positionOfRelevantOperator = positionOfFirstTopLevelPower;
        }



        // Make two Token lists before and after the expression, parse each as expression
        toReturn.astExpression1 = parseAstExpression(new ArrayList<>(tokenList.subList(0, positionOfRelevantOperator)));
        toReturn.astOperator = parseOperator(tokenList.get(positionOfRelevantOperator));
        toReturn.astExpression2 = parseAstExpression(new ArrayList<>(tokenList.subList(positionOfRelevantOperator + 1, tokenList.size())));


        return toReturn;
    }

    private AstOperator parseOperator (Token operator) {

        AstOperator toReturn = new AstOperator();
        toReturn.astOperatorContent = operator.tokenString;

        return toReturn;
    }

    private AstValue parseValue (ArrayList<Token> valueTokens) {

        AstValue toReturn = new AstValue();

        boolean containsSpecialToken = false;

        // check if value contains special token
        for(Token t : valueTokens) {

            if(t.getTokenType().equals(TokenType.special)) {

                containsSpecialToken = true;

            }

        }

        if(containsSpecialToken) {

            toReturn.astDecimal = parseDecimal(valueTokens);

        } else {

            // Check if token is number or variable and parse accordingly

            if(valueTokens.get(0).getTokenType().equals(TokenType.var)) {

                toReturn.astVariable = parseVariable(valueTokens);

            } else {
                toReturn.astNumber = parseNumber(valueTokens);
            }


        }
        return toReturn;
    }


    private AstVariable parseVariable (ArrayList<Token> valueTokens) {
        AstVariable toReturn = new AstVariable();

        toReturn.astVariable = valueTokens.get(0).getTokenString();

        return toReturn;
    }

    private AstDecimal parseDecimal (ArrayList<Token> decimalTokens) {

        AstDecimal toReturn = new AstDecimal();

        int i = 0;
        int positionOfDecimalPoint = -1;

        // determines position of dot
        for(Token t : decimalTokens) {

            if(t.getTokenType().equals(TokenType.special)) {
                positionOfDecimalPoint = i;
                break;
            }

            i++;

        }


        // parse digits before comma as list and save it into toReturn.before

        // if decimal point is in second position (e.g. 1.276) make a new List with one token (.subList operation does not support subList with same index twice
        if (positionOfDecimalPoint == 1) {

            ArrayList<Token> temp = new ArrayList<>();
            temp.add(decimalTokens.get(0));
            toReturn.astDigitBeforeComma = parseNumber(temp);

        } else {

            try {

                toReturn.astDigitBeforeComma = parseNumber(new ArrayList<>(decimalTokens.subList(0, positionOfDecimalPoint - 1)));

            } catch (IllegalArgumentException illegalArgumentException) {
                throw new Exceptions.ParserException("String contains invalid char");
            }

        }


        // same as above but check if for 3247.3
        if (positionOfDecimalPoint +  1 - decimalTokens.size() == 0) {

            ArrayList<Token> temp = new ArrayList<>();
            temp.add(decimalTokens.get(decimalTokens.size() - 1));
            toReturn.astDigitAfterComma = parseNumber(temp);

        } else {

            toReturn.astDigitAfterComma = parseNumber(new ArrayList<> (decimalTokens.subList(positionOfDecimalPoint +  1, decimalTokens.size())));

        }




        return toReturn;
    }

    private AstNumber parseNumber (ArrayList<Token> numberTokens) {

        AstNumber toReturn = new AstNumber();

        // take first Token from list and parse as digit without zero

        toReturn.astDigitWoz = parseDigitWoZ(numberTokens.get(0));

        if(numberTokens.size() == 1) {
            return toReturn;
        }



        // parse rest as regular digit
        numberTokens.stream()
                .skip(1)
                .forEach(token -> {
                    AstDigit temp = parseAstDigit(token);
                    toReturn.astDigits.add(temp);
                });

        return toReturn;

    }

    private AstDigitWoZ parseDigitWoZ (Token digitWoZ) {
        require(!Objects.equals(digitWoZ.tokenString, "0"));

        AstDigitWoZ toReturn = new AstDigitWoZ();

        int tokenContentAsString;

        try {
            tokenContentAsString = Integer.parseInt(digitWoZ.tokenString);
        } catch (NumberFormatException error) {
            System.out.println("Can't parse string in AstDigitWoZ");
            throw error;
        }


        toReturn.astDigitWozContent = tokenContentAsString;

        return toReturn;
    }

    private AstDigit parseAstDigit (Token digit) {

        AstDigit toReturn = new AstDigit();

        int tokenContentAsString;



        try {
            tokenContentAsString = Integer.parseInt(digit.tokenString);
        } catch (NumberFormatException error) {
            System.out.println("Can't parse string in AstDigit");
            throw error;
        }



        if(tokenContentAsString == 0) {
            toReturn.astDigit = 0;
            return toReturn;
        }

        toReturn.astDigitWoZ = parseDigitWoZ(digit);

        return toReturn;
    }


}
