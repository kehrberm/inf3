import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {

        // Get expressions from file and store them in a string list

        ArrayList<String> stringsFromFile = new ArrayList<>();

        String filePath = "./Aufgabe4/src/expressions.txt";

        Scanner scan = null;
        try {

            scan = new Scanner(new File(filePath));

            while (scan.hasNext()) {

                stringsFromFile.add(scan.nextLine());

            }

            scan.close();

        } catch (FileNotFoundException | NullPointerException e) {
            e.printStackTrace();
        }


        // Lex the strings in the string list

        Lexer lexer = new Lexer();

        ArrayList<ArrayList<Token>> tokenListList = new ArrayList<>();


        for(String s : stringsFromFile) {

            tokenListList.add(lexer.lex(s));

        }

        // Parse the tokens in the token list


        Parser p = new Parser();

        ArrayList<AstExpression> rootAstExpressions = new ArrayList<>();


        for(ArrayList<Token> tokenList : tokenListList) {

            try {
                rootAstExpressions.add(p.parse(tokenList));
            } catch (Exception e) {
                System.out.println("Invalid function received");
            }

        }

        // evaluate the ast and get the values f(x) from -20 to 20 in steps of one
        // these get written into an integer array

        Evaluator e = new Evaluator();

        ArrayList<int[]> arraysOfXValues = new ArrayList<>();

        for(AstExpression astExpression : rootAstExpressions) {

            int[] xCords = new int[41];

            int j = 0;

            for(int i = -20; i < 21; i++) {
                e.setCounter(i);

                xCords[j] = (int) e.visit(astExpression);

                System.out.println(e.visit(astExpression));
                j++;
            }

            arraysOfXValues.add(xCords);

            System.out.println("------------");

        }

        // creates ValuesToDraw object to transfer data to plotter

        ValuesToDraw valuesToDraw = new ValuesToDraw(arraysOfXValues);


        Plotter.plot(valuesToDraw);


    }
}
