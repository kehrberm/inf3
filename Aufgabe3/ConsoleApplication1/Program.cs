using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Mime;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Messaging;
using System.Threading;

namespace ConsoleApplication1 
{

    public static

        class Program
    {
        public static void Main(string[] args)
        {

            int[] argsInt = getArgsAsIntArr(args);

            

            Buffer buffer = new Buffer();
            
            

            createNConsumers(argsInt[0], buffer);
            createNProducers(argsInt[1], buffer);
            
        }

        public static int[] getArgsAsIntArr(string[] args)
        {
            int nrProd = 0;
            int nrCons = 0;

            //Parse received program arguments to int
            try
            {
                nrProd = int.Parse(args[0]);
                nrCons = int.Parse(args[1]);
            }
            catch (FormatException ignored)
            {
                Console.WriteLine("Please provide two positive integers as program arguments");
                Environment.Exit(1);
            }

            Debug.Assert(nrProd > -1 && nrCons > -1);

            return new [] { nrProd, nrCons };
        }

        public static void createNConsumers(int n, Buffer buffer)
        {
            //Create n new producers where n = received argument
            for (int i = 0; i < n; i++)
            {
                //create new Producer object
                Producer p = new Producer(buffer);

                //create new Thread and start it
                Thread newThread = new Thread(p.Produce);
                newThread.Start();
            }
        }

        public static void createNProducers(int n, Buffer buffer)
        {
            //Create n new consumers where n = received argument
            for (int i = 0; i < n; i++)
            {
                //create new Consumer object
                Consumer c = new Consumer(buffer);

                //create new Thread and start it
                Thread newThread = new Thread(c.Consume);
                newThread.Start();

            }
        }
    }
}
    



