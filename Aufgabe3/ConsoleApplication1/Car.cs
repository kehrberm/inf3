
using System;

namespace ConsoleApplication1
{


//Class for the objects which get stored in the buffer from the class Producer and deleted from the class Consumer
    public class Car
    {
        static int carId = 0;
        private int thisCarId;

        // Constructor which sets the car id and increments ++ the carId for the next object
        public Car()
        {
            thisCarId = carId++;
            Console.WriteLine("car created. id = " + thisCarId);
        }
        
        // Destructor which decrements -- the carId
        ~Car()
        {
            carId--;
            Console.WriteLine("car deleted. id = " + thisCarId);
        }
        
        // Returns the carId for Consumer and Producer so they can get the carId
        public int getThisCarId()
        {
            return thisCarId;
        }


    }
}
