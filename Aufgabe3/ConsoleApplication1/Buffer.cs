using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Threading;

namespace ConsoleApplication1
{
    
    public class Buffer
    {
        private Queue<Car> _carQueue;
        private int _size = 10;
        private Mutex _mutex;

        // Constructor initialized with a Mutex and a Queue.
        public Buffer()
        {
            _mutex = new Mutex();
            _carQueue = new Queue<Car>();
        }

        // This function adds a Car Object into the Queue.
        public void Push(Car c)
        {
            // Check whether the Queue is full and throws an Exception if so.
            if (_carQueue.Count == _size)
            {
                throw new QueueFullException("Buffer full");
            }  
            
            _carQueue.Enqueue(c);
        }

        // Removes a Car Object from the Queue and returns the Queue.
        public Car Pop()
        {
            if (_carQueue.Count == 0)
            {
                throw new QueueEmptyException("Buffer empty");
            }

            return _carQueue.Dequeue();
        }

        // Check if the Queue is full.
        public bool Full()
        {
            return _carQueue.Count == _size;
        }

        // Check if the Queue is empty.
        public bool Empty()
        {
            return _carQueue.Count == 0;
        }

        // returns the Mutex. 
        public Mutex GetMutex()
        {
            return this._mutex;
        }
    }
    
}

public class QueueEmptyException : Exception
{
    public QueueEmptyException(string bufferEmpty)
        : base (bufferEmpty) { }
}

public class QueueFullException : Exception
{
    public QueueFullException(string bufferEmpty)
        : base (bufferEmpty) { }
}
