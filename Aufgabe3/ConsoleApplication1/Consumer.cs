using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.ConstrainedExecution;
using System.Threading;
using NUnit.Framework;


namespace ConsoleApplication1
{
    public class Consumer
    {
        private Buffer _buffer;
        private Random _random;
        private bool _isAsleep;
        private static bool _wakeConsumersUp;

        public Consumer(Buffer buffer)
        {
            _buffer = buffer;
            _random = new Random();
        }
        
        [SuppressMessage("ReSharper", "FunctionNeverReturns")]
        public void Consume()
        {
            while (true)
            {
                //if isAsleep = true: method checks if wakeConsumersUp has been set to true every 50ms
                if (_isAsleep)
                {
                    Console.WriteLine("consumer asleep...");
                    Thread.Sleep(50);

                    //if wakeConsumersUp = true is asAsleep is set to true and if(isAsleep) "loop" is exited on next iteration
                    if (_wakeConsumersUp)
                    {
                        Console.WriteLine("consumer was woken up");
                        _isAsleep = false;
                    }
                }
                //if consumer isnt asleep: consumer gets mutex from buffer, wakes up producers if buffer is full and removes cars if consumer is not empty
                else
                {
                    Thread.Sleep(_random.Next(500, 1500));
                    Mutex m = _buffer.GetMutex();
                    
                    Debug.Assert(m != null);
                    
                    m.WaitOne();

                    if (_buffer.Full())
                    {
                        Producer.WakeUp();
                    }
                    
                    if (!_buffer.Empty())
                    {
                        Car c = _buffer.Pop();
                        Console.WriteLine("car pulled out of parking space. car id = " + c.getThisCarId());
                    }

                    //if buffer is empty: consumers are set to sleep, own instance is set to sleep
                    else
                    {
                        _wakeConsumersUp = false;
                        _isAsleep = true;
                    }

                    m.ReleaseMutex();

                }
            }
        }

        //this consumer instance can be woken up by calling this method
        public static void WakeUp()
        {
            _wakeConsumersUp = true;

        }
    }
}
