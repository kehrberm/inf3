﻿using System;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;

namespace ConsoleApplication1.Test
{
    
    [TestFixture]
    public class TestSuccess 
    {
        Buffer buffer = new Buffer();
        private int _size = 10;
        private Mutex _mutex;
        private Queue<Car> _carQueue = new Queue<Car>();
        Car _car;

        [TestCase]
        public void PushSuccessful()
        {
            for (int i = 0; i < 10 ; i++)
            {
                buffer.Push(_car);
            }
            Assert.AreEqual(true,buffer.Full());
        }

        [TestCase]
        public void PopSuccessful()
        {
            for (int i = 0; i <= 9 ; i++)
            {
                buffer.Push(_car);
            }
            
            for (int i = 0; i <= 9 ; i++)
            {
                buffer.Pop();
            }
            Assert.AreEqual(true,buffer.Empty());
        }

        [TestCase]
        public void MoreConsumersThanProducers()
        {
            string[] args = { "0", "15"};
            Assert.DoesNotThrow(() => Program.Main(args));
        }
    }
}