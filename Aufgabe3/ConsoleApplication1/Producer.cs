using System;
using System.Threading;

namespace ConsoleApplication1
{
    
    public class Producer
    {
        private Buffer _buffer;
        private Random _random;
        private bool _isAsleep;
        private static bool _wakeProducersUp;


        public Producer(Buffer buffer)
        {
            _random = new Random();
            _buffer = buffer;
        }
        
        
        public void Produce()
        {
            while (true)
            {
                //if isAsleep = true: method checks if wakeProducersUp has been set to true every 50ms
                if (_isAsleep)
                {
                    Console.WriteLine("producer asleep...");
                    Thread.Sleep(50);

                    //if wakeProducersUp = true is asAsleep is set to true and if(isAsleep) "loop" is exited on next iteration
                    if (_wakeProducersUp)
                    {
                        Console.WriteLine("producer was woken up");
                        _isAsleep = false;
                    }
                }
                //if producer isnt asleep: producer gets mutex from buffer, wakes up consumers if buffer is empty and removes cars if producers is not full
                else
                {
                    Thread.Sleep(_random.Next(500, 1500));
                    Mutex m = _buffer.GetMutex();
                    m.WaitOne();

                    if (_buffer.Empty())
                    {
                        Consumer.WakeUp();
                    }

                    if (!_buffer.Full())
                    {
                        Car c = new Car();
                        _buffer.Push(c);
                        Console.WriteLine("car " + c.getThisCarId() + " added");
                    }
                    //if buffer is full: producers are set to sleep, own instance is set to sleep
                    else
                    {
                        _wakeProducersUp = false;
                        _isAsleep = true;
                        
                    }

                    m.ReleaseMutex();

                }
            }
        }

        //this consumer instance can be woken up by calling this method
        public static void WakeUp()
        {
            _wakeProducersUp = true;
        }
    }
    
}
