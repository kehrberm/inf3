
//Aufgabe 2

#include <iostream>

using namespace std;

class Cards {

private:
    string name;
    string mana;
    string cmc;
    string type;
    string count;

public:
    //setter
    void setName(string name) {
        this->name = name;
    }
    //getter Name
    string getName() {
        return name;
    }

    void setMana(string mana) {
        this->mana = mana;
    }

    string getMana() {
        return mana;
    }

    void setCount(string count) {
        this->count = count;
    }

    string getCount() {
        return count;
    }

    void setCmc(string cmc) {
        this->cmc = cmc;
    }

    string getCmc() {
        return cmc;
    }

    void setType(string type) {
        this->type = type;
    }

    string getType() {
        return type;
    }
};
