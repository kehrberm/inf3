

#include <iostream>
#include <string>
#include <iostream>
#include <fstream>
#include <list>
#include <string>
#include <fstream>
#include <cstring>
#include <assert.h>
 
 //from https://www.techiedelight.com/check-if-a-string-ends-with-another-string-in-cpp/
bool endsWith(std::string const &str, std::string const &suffix) {
    if (str.length() < suffix.length()) {
        return false;
    }
    return std::equal(suffix.rbegin(), suffix.rend(), str.rbegin());
}
//https://stackoverflow.com/questions/2390912/checking-for-an-empty-file-in-c
bool isEmpty(std::ifstream& pFile)
{
    return pFile.peek() == std::ifstream::traits_type::eof();
}