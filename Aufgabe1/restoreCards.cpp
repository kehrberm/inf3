#include <iostream>
#include <fstream>
#include <list>
#include <string>
#include <fstream>
#include <cstring>
#include <assert.h>
#include "helpMethods.cpp"
#include "Cards.cpp"

using namespace std;

class restoreCards {

    private:

        const float L_TOLERANCE = 0.2675;

        int totalCardsAmount;
        int repairedCardsAmount;

        string fileName;
        string referenceFileName;


        list<Cards> cardList;
        list<string> referenceList;
        list<Cards> repairedCards;

    public:

        restoreCards(string fileName, string referenceFileName) {

            assert(endsWith(fileName, ".txt"));
            this->fileName = fileName;

            assert(endsWith(fileName, ".txt"));
            this->referenceFileName = referenceFileName;
        };

        void restore() {

        
            readScrambled();
            readReference();


            restoreIndividualCards();


            writeListToFile(repairedCards, "repairedCards.txt");

            //Aufgabe 4
            writeListToFile(cardList, "scrambledList.txt");

            printRepairStats();


        };

    private:

        int computeLevenshteinDistance(string zeile, string spalte) {

            // Initialise a 2d Matrix with the length of the word
            int M[zeile.length() + 1][spalte.length() + 1];

            for (int ZEILE = 0; ZEILE < zeile.length() + 1; ZEILE++) {
                for (int SPALTE = 0; SPALTE < spalte.length() + 1; SPALTE++) {

                    M[ZEILE][SPALTE] = 0;

                    if (ZEILE == 0) {
                        M[ZEILE][SPALTE] = SPALTE;
                    }

                    if (SPALTE == 0) {
                        M[ZEILE][SPALTE] = ZEILE;
                    }
                }
            }

            for (int ZEILE = 1; ZEILE < zeile.length() + 1; ZEILE++) {
                for (int SPALTE = 1; SPALTE < spalte.length() + 1; SPALTE++) {

                    int c = 1;
                    if (zeile[ZEILE - 1] == spalte[SPALTE - 1]) {
                        c = 0;
                    }

                    int rep = M[ZEILE - 1][SPALTE - 1] + c;
                    int ins = M[ZEILE][SPALTE - 1] + 1;
                    int del = M[ZEILE - 1][SPALTE] + 1;

                    M[ZEILE][SPALTE] = min(rep, min(ins, del));
                }
            }
            return M [zeile.length()][spalte.length()];

        }

        void readScrambled() {

            ifstream scrambledFile(fileName);

            assert(!isEmpty(scrambledFile));

            string singleLine;


                //Aufgabe 3
            while (getline(scrambledFile, singleLine)) {
                

                totalCardsAmount++;

                //string singleLine copied in char Array
                int n = singleLine.length();
                char singleLineArray[n + 1];
                strcpy(singleLineArray, singleLine.c_str());


                //singleLineArray split in substrings
                char *singleLineSubstring = strtok(singleLineArray, "|");
                string cardInfoArr[5];

                //Logic for seperating the Substrings
                int j = 0;
                while (singleLineSubstring) {

                    //Substring (Kartenattribute) werden in cardInfoArrgeschrieben
                    cardInfoArr[j] = singleLineSubstring;
                    j++;
                    singleLineSubstring = strtok(nullptr, "|");
                }

                //initalising new Card Object
                Cards newCard;

                //setting Card Attribute
                newCard.setName(cardInfoArr[0]);
                newCard.setMana(cardInfoArr[1]);
                newCard.setCmc(cardInfoArr[2]);
                newCard.setType(cardInfoArr[3]);
                newCard.setCount(cardInfoArr[4]);

                //add Card in card List
                cardList.push_back(newCard);
            }

        };

        void readReference() {
            ifstream reference(referenceFileName);

            string singleLine;

            while (getline(reference, singleLine)) {
                referenceList.push_back(singleLine);
            }
            reference.close();
        }

        void writeListToFile(list<Cards> listToWrite, string destinationFileName) {

            ofstream cardWrite(destinationFileName);

            for(Cards c : listToWrite) {
                cardWrite << c.getName() << "|" << c.getMana() << "|" << c.getCmc() << "|" << c.getType() << "|" << c.getCount() << "\n";
            }

            cardWrite.close();

        }

        void restoreIndividualCards() {
            for (Cards card : cardList) {
                for (string str : referenceList) {
                    int l_dist = computeLevenshteinDistance(card.getName(), str);

                    if(l_dist < card.getName().length() * L_TOLERANCE) {
                        cout << "Repaired card: " << str << endl;
                        repairedCardsAmount++;

                        card.setName(str);
                        repairedCards.push_back(card);
                        break;
                    }
                }
            }
        }

        void printRepairStats(){

            cout << "scrambled cards: " << totalCardsAmount << endl;
            cout << "repaired cards: " << repairedCardsAmount << endl;
        }

};